# Projet XML

---

## Membres

- Patrick Chang 71802046

- Vincent Du    71801856

---

- ## Attention

Le fichier CSV `base_ratp.csv` et le fichier JAR `saxon-he-10.3.jar` ne sont pas dans l'archive du projet pour respecter la forme du rendu, mais sont toutefois utilisés pour générer les fichiers `metro.xml` `metro.xsl` respectivement.

---

- ## Convertisseur CSV en XML

On souhaite convertir le fichier `base_ratp.csv` en un fichier
au format XML `metro.xml`.

On utilise la classe `MetroConverter.java` en compilant avec :

`javac MetroConverter.java`

Puis en exécutant avec :

`java MetroConverter base_ratp.csv metro.xml`

---

- ## Schéma XML (XSD)

Pour vérifier notre schéma XML, on lance la commande :

`xmllint --schema metro.xsd metro.xml > out.txt`

Le fichier est valide si cette commande n'affiche rien, ou la ligne `metro.xml validates`.

Notre fichier respecte les contraintes suivantes :

- aucun partage d’identifiant, ou d’emplacement (couple formé par la latitude et la longitude) entre deux stations distinctes

- aucun partage d’identifiant entre deux lignes de métro distinctes

- aucun doublon d'attribut `ligne` de `<correspondance>` pour une même `<station>`

---

- ## Convertisseur XML en SVG

Le fichier `metro.xsl` sert à convertir le fichier `metro.xml` en un format svg qui représente une seule ligne de métro.

Pour obtenir une ligne de métro, il vous faut entrer la commande :

`java -jar saxon-he-10.3.jar -s:metro.xml -xsl:metro.xsl -o:ligne.svg l=LINE`

Où on remplace `LINE` par le nom de la ligne, ce qui donne le fichier `ligne.svg`.

Par exemple si on souhaite obtenir la ligne `3B`, on entre la commande :

`java -jar saxon-he-10.3.jar -s:metro.xml -xsl:metro.xsl -o:ligne.svg l=3B`

---

### Extraire toutes les lignes

Nous avons un petit script en bash qui crée toute les lignes en SVG possibles dans un répertoire `metro_lines`.

Il suffit d'entrer la commande `./extract_lines` pour extraire les lignes de métro vers le répertoire ci-dessus.

On peut ensuite comparer les lignes créees dans le répertoire `metro_lines` avec les lignes de métro réelles de la ratp qui sont stockées dans le répertoire `ratp_metro_lines`.