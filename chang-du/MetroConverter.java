import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

/**
 * This class will convert the CSV file base_ratp.csv
 * into an XML file with a format chosen for the Parisian subway
 */
class MetroConverter {
    private final static String sep = ";";  // Separator
    private static int indentation = 0;     // Number of indentations
    private static String spaces = "";      // Spaces for indentation

    // station_id -> List<service_short_name> (metro line)
    private static HashMap<String, ArrayList<String>> corresp = new HashMap<>();

    // Indexes of data in an array
    private final static int station_id         = 0;
    private final static int station_name       = 1;
    private final static int station_desc       = 2;
    private final static int station_lat        = 3;
    private final static int station_lon        = 4;
    private final static int stop_sequence      = 5;
    private final static int route_id           = 6;
    private final static int service_id         = 7;
    private final static int direction_id       = 8;
    private final static int service_short_name = 9;
    private final static int long_name_first    = 10;
    private final static int long_name_last     = 11;


    /**
     * Increases indentation by 1
     * Updates spaces
     */
    private static void addIndent() {
        indentation += 1;
        spaces += "  "; // 2 spaces indentation
    }

    /**
     * Reduces indentation by 1
     * Updates spaces
     */
    private static void reduceIndent() {
        indentation = indentation > 0 ? indentation - 1 : 0;
        spaces = "";

        for (int i = 0; i < indentation; i++)
            spaces += "  "; // 2 spaces indentation
    }

    /**
     * @return Header of an XML file
     */
    public static String headerXML() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    }

    /**
     * <tagName attr[0] attr[1] ...>
     * @param tagName
     * @param attr
     * @return String with the tag
     */
    public static String openTagXML(String tagName, String[] attr) {
        String str_attr = "";

        if (attr != null) {
            for (String a : attr)
                str_attr += " " + a;
        }

        return spaces + "<" + tagName + str_attr + ">";
    }

    /**
     * </tagName>
     * @param tagName
     * @return String with the tag
     */
    public static String closeTagXML(String tagName) {
        return spaces + "</" + tagName + ">\n";
    }

    /**
     * <tagName attr[0] attr[1] ... />
     * @param tagName
     * @param attr
     * @return String with the tag
     */
    public static String singleTagXML(String tagName, String[] attr) {
        String str_attr = "";

        if (attr != null) {
            for (String a : attr)
                str_attr += " " + a;
        }

        return spaces + "<" + tagName + str_attr + "/>\n";
    }

    public static String generateAttr(String name, String value) {
        return name + "=" + "\"" + value + "\"";
    }

    /**
     * Prepares the String to write in the XML file for a station
     * @param station The station
     * @return String for a metro station with all the tags and date
     */
    public static String stationXML(String[] station) {
        String str = "";
        String[] attrStation =
            { generateAttr("id", station[station_id]) };
        ArrayList<String> stCorresp = corresp.get(station[station_id]);

        // Open tag <station id="">
        str += openTagXML("station", attrStation) + "\n";
        addIndent();

        // Tag <nom>
        str += openTagXML("nom", null) + "\n";
        addIndent();
        str += spaces + station[station_name] + "\n";
        reduceIndent();
        str += closeTagXML("nom");

        // Tags <correspondance ligne=""/>
        for (String cor : stCorresp) {
            String[] attrCor = { generateAttr("ligne", cor) };

            // Only write external correspondances
            if (!cor.equals(station[service_short_name]))
                str += singleTagXML("correspondance", attrCor);
        }

        // Prepare attributes for <adresse>
        String[] attrAdr = {
            generateAttr("lat", station[station_lat]),
            generateAttr("lon", station[station_lon])
        };

        // Tag <adresse lat="" lon="">
        str += openTagXML("adresse", attrAdr) + "\n";
        addIndent();
        str += spaces + station[station_desc] + "\n";
        reduceIndent();
        str += closeTagXML("adresse");

        // Close tag </station>
        reduceIndent();
        str += closeTagXML("station") + "\n";

        return str;
    }

    /**
     * Prepares the String to write in the XML file for a group of station
     * One of the top or bottom station can be null
     * @param top station data
     * @param bot station data
     * @param style Group style : split, join, parallel
     * @return the group String in XML format
     */
    public static String groupXML(String[] top, String[] bot, String style) {
        String str = "";
        String[] attrGrp = { generateAttr("style", style) };

        // Open tag <group style="">
        str += openTagXML("group", attrGrp) + "\n";
        addIndent();

        // Add Top station
        if (top != null)
            str += stationXML(top);
        else
            str += singleTagXML("none", null) + "\n";

        // Add Bottom station
        if (bot != null)
            str += stationXML(bot);
        else
            str += singleTagXML("none", null);

        // Close tag </group>
        reduceIndent();
        str += closeTagXML("group") + "\n";

        return str;
    }

    /**
     * Prepares the String to write in the XML file for a metro line
     * @param mLine Metro line with all directions
     * @return String for a metro line with all the tags and data
     */
    public static String metroLineXML(ArrayList<ArrayList<String[]>> line) {
        String str = "";
        String currentLineID = line.get(0).get(0)[service_short_name];
        String[] attrLine = { generateAttr("id", currentLineID) };
        HashMap<String, Integer> occ = getMetroLineOcc(line);
        String firstPos = line.get(0).get(0)[stop_sequence];
        String style = "parallel";  // Used in case of a <group> tag
        boolean hasStation0 = true; // Has station direction 0
        boolean hasStation1 = true; // Has station direction 1
        boolean isSplit = true;     // default is true in case we start with bifurcation
        String route_id0 = "";      // route_id for the direction 0
        String route_id1 = "";      // route_id for the direction 1
        int nbPaths = 0;            // Number of paths : a normal station has nbPaths == occ(station)
        int d0 = 0;                 // Direction number
        int d1 = 1;                 // Direction number
        int i = 0;                  // Index in the line
        int j = 0;                  // Index in the line

        // Get nbPaths in one direction
        while (i < line.get(d0).size() &&
            line.get(d0).get(i)[stop_sequence].equals(firstPos))
        {
            nbPaths++;
            i++;
        }
        nbPaths *= 2;

        // Open tag <ligne id="">
        str += openTagXML("ligne", attrLine) + "\n";
        addIndent();

        /*
            Setup indexes and directions we read.
            In case of metro line 7 and 13 where nbPaths == 4
            We only check the direction 0 which contains paths in pair
         */
        d0 = 0;
        d1 = nbPaths == 2 ? 1 : 0;
        i = 0;
        j = d1 == 1 ? 0 : 1; // j starts at 1 if we read the same direction
        hasStation0 = i < line.get(d0).size();
        hasStation1 = j < line.get(d1).size();

        // Save the route_id to not mess with the <group> station order
        route_id0 = line.get(d0).get(i)[route_id];
        route_id1 = line.get(d1).get(j)[route_id];

        /*
            There's always only 2 directions
            We compare both directions' stations at the same pos
            to distinguish between a <group> and a normal <station>
         */
        while (hasStation0 || hasStation1) {
            int occ0 = hasStation0 ? occ.get(line.get(d0).get(i)[station_id]) : 0;
            int occ1 = hasStation1 ? occ.get(line.get(d1).get(j)[station_id]) : 0;
            String[] st0 = null; // Station direction 0 (route_id0)
            String[] st1 = null; // Station direction 1 (route_id1)

            // Get the style in case we have a <group>
            if (isSplit)
                style = "parallel";
            else {
                style = "split";
                isSplit = true;
            }

            // Differentiate a <group> and a <station> tag
            if (hasStation0 && hasStation1) // Both directions still have a station
            {
                // Normal <station> on both directions
                if (occ0 == nbPaths && occ1 == nbPaths) {
                    str += stationXML(line.get(d0).get(i));
                    isSplit = false;
                    i += d1 == 1 ? 1 : 2;
                    j += d1 == 1 ? 1 : 2;

                } else { // It is a <group> tag
                    if (occ0 == nbPaths / 2) { // Direction 0 is in the group
                        // Keep top-bot station order by route_id
                        if (line.get(d0).get(i)[route_id].equals(route_id0))
                            st1 = line.get(d0).get(i);
                        else
                            st0 = line.get(d0).get(i);

                        i += d1 == 1 ? 1 : 2;
                    }
                    if (occ1 == nbPaths / 2) { // Direction 1 is in the group
                        // Keep top-bot station order by route_id
                        if (line.get(d1).get(j)[route_id].equals(route_id1))
                            st0 = line.get(d1).get(j);
                        else
                            st1 = line.get(d1).get(j);

                        j += d1 == 1 ? 1 : 2;
                    }

                    isSplit = true;
                    str += groupXML(st0, st1, style);
                }
            }
            else // One of the 2 directions reached its last station : <group>
            {
                if (!hasStation0) { // Direction 0 ended
                    // Keep top-bot station order by route_id
                    if (line.get(d1).get(j)[route_id].equals(route_id1))
                        st0 = line.get(d1).get(j);
                    else
                        st1 = line.get(d1).get(j);

                    j += d1 == 1 ? 1 : 2;
                }
                else { // Direction 1 ended
                    // Keep top-bot station order by route_id
                    if (line.get(d0).get(i)[route_id].equals(route_id0))
                        st1 = line.get(d0).get(i);
                    else
                        st0 = line.get(d0).get(i);

                    i += d1 == 1 ? 1 : 2;
                }

                isSplit = true;
                str += groupXML(st0, st1, style);
            }

            hasStation0 = i < line.get(d0).size();
            hasStation1 = j < line.get(d1).size();
        }

        // Close tag </ligne>
        reduceIndent();
        str += closeTagXML("ligne") + "\n";

        return str;
    }

    /**
     * Fills the Hashmap corresp with every metro station_id
     * linked to all its service_short_name
     * @param lines
     */
    public static void fillCorrespondances(ArrayList<ArrayList<String[]>> lines) {
        for (int i = 0; i < lines.size(); i++) {
            for (int j = 0; j < lines.get(i).size(); j++) {
                String[] station = lines.get(i).get(j);

                // Station not registered yet
                if (!corresp.containsKey(station[station_id])) {
                    corresp.put(station[station_id], new ArrayList<String>());
                    corresp.get(station[station_id]).add(station[service_short_name]);
                } else {
                    // Metro Line not registered for this station
                    if (!corresp.get(station[station_id])
                        .contains(station[service_short_name]))
                    {
                        corresp.get(station[station_id]).add(station[service_short_name]);
                    }
                }
            }
        }
    }

    /**
     * Stores in a HashMap<String, Integer> the occurences
     * of each station_id in a metro line
     * @param line Metro line
     * @return The occurences of each station in a metro line
     */
    public static HashMap<String, Integer>
        getMetroLineOcc(ArrayList<ArrayList<String[]>> line)
    {
        HashMap<String, Integer> occ = new HashMap<>();

        // For each direction
        for (ArrayList<String[]> d : line) {
            for (String[] station : d) {
                int curOcc = occ.containsKey(station[station_id]) ?
                        occ.get(station[station_id]) : 0;

                occ.put(station[station_id], curOcc + 1);
            }
        }

        return occ;
    }

    /**
     * Orders the different directions in a metro line
     * so that both directions start with the same station
     * @param metro All metro lines
     * @return The metro lines with ordered station list
     */
    public static ArrayList<ArrayList<String[]>>
        reverseDirections(ArrayList<ArrayList<String[]>> metro)
    {
        // Put different directions in the same order
        for (int i = 0; i < metro.size(); i++) {
            ArrayList<String[]> linePath = metro.get(i);

            if (linePath.get(0)[direction_id].equals("1"))
                Collections.reverse(linePath);
        }
        return metro;
    }


    /**
     * Gets the metro line from the list of metro lines
     * @param metro All metro lines and directions
     * @param lineNb service_short_name
     * @return the corresponding Metro line with its different directions
     */
    public static ArrayList<ArrayList<String[]>>
        getMetroLine(ArrayList<ArrayList<String[]>> metro, String lineNb)
    {
        ArrayList<ArrayList<String[]>> mLine = new ArrayList<>();

        for (int i = 0; i < metro.size(); i++) {
            if (metro.get(i).get(0)[service_short_name].equals(lineNb))
                mLine.add(metro.get(i)); // Add a direction of a metro line
        }

        return mLine;
    }

    /**
     * Takes 2 file names (.csv and .xml) as arguments
     * Reads the CSV file with a fixed format
     * and writes into an XML file with a predetermined format
     * @param args
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println(
                "Usage : java MetroConverter [fileInput].csv [fileOutput].xml");
            return;
        }

        // Files setup
        File fileReader = new File(args[0]);
        FileWriter fileWriter = null;
        Scanner scan = null;

        // All metro lines : Each index is a metro line
        // And each metro line's List contains its stations data
        ArrayList<ArrayList<String[]>> metroLines =
            new ArrayList<ArrayList<String[]>>();

        int idx = -1;
        String last_service_id = "";
        String last_direction_id = "";

        try {
            fileWriter = new FileWriter(args[1]);
			scan = new Scanner(fileReader);

            // Skip first line
            if (scan.hasNextLine())
                scan.nextLine();

            // Add header XML
            fileWriter.write(headerXML());

			// Read the file line by line
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
                String[] station = line.split(sep); // sep = ";"

                // Metro line changed or direction changed
                if (!last_service_id.equals(station[service_id]) ||
                    !last_direction_id.equals(station[direction_id]))
                {
                    last_service_id = station[service_id];
                    last_direction_id = station[direction_id];
                    idx++;

                    metroLines.add(new ArrayList<String[]>());
                }
                metroLines.get(idx).add(station);
            }

            // Setup metroLines
            fillCorrespondances(metroLines);
            metroLines = reverseDirections(metroLines);

            String lastMetroLine = "";

            // Open tag <metro>
            fileWriter.write(openTagXML("metro", null) + "\n");
            addIndent();

            // For each metro lines, write the line in the XML file
            for (int i = 0; i < metroLines.size(); i++) {
                // Only write when we change line name (service_short_name)
                if (!lastMetroLine.equals(metroLines.get(i).get(0)[service_short_name])) {
                    lastMetroLine  = metroLines.get(i).get(0)[service_short_name];
                    fileWriter.write(
                        metroLineXML(getMetroLine(metroLines, lastMetroLine))
                    );
                }
            }

            // Close tag </metro>
            reduceIndent();
            fileWriter.write(closeTagXML("metro"));

            // Close Files
            scan.close();
            fileWriter.close();

		} catch (Exception e) {
            e.printStackTrace();
		}
    }
}