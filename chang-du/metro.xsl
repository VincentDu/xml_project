<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:fun="foobar"
  exclude-result-prefixes="fun xsd">

  <xsl:output indent="yes"/>

  <!-- Parameter in command -->
  <xsl:param name="l"/> <!-- Metro line name -->

  <!-- Metro Line color -->
  <xsl:variable name="c" select="fun:getColor($l)"/>

  <!-- Variables SVG -->
  <xsl:variable name="svg-width"
      select="100 * count(//ligne[@id = $l]/*)"/> <!-- Frame's width -->
  <xsl:variable name="svg-height" select="800"/>  <!-- Frame's height -->
  <xsl:variable name="midY" select="0.5 * $svg-height"/>  <!-- Frame's middle y-axis -->
  <xsl:variable name="gapX" select="80"/>   <!-- Gap between each position -->
  <xsl:variable name="gapY" select="100"/>  <!-- Gap between each bifurcation-->
  <xsl:variable name="x" select="14"/>      <!-- Extra size to add or remove -->
  <xsl:variable name="r" select="10"/>      <!-- Circle radius -->


  <!-- Root template -->
  <xsl:template match="/">
    <svg width="{$svg-width}" height="{$svg-height}">
      <!-- Line number logo on top left -->
      <circle cx="50" cy="50" r="30"
              fill="{$c}" stroke="{$c}" stroke-width="5"/>
      <text x="{38 - 12 * (string-length($l)-1)}" y="62"
            font-family="serif" font-weight="bold" font-size="32" fill="black">
        <xsl:value-of select="$l"/>
      </text>

      <!-- Call all child elements of the line in parameter -->
      <xsl:apply-templates select="//ligne[@id = $l]/*"/>
    </svg>
  </xsl:template>


  <!-- Station template -->
  <xsl:template match="station">
    <xsl:call-template name="drawStation">
      <xsl:with-param name="pos" select="position()" as="xsd:integer"/>
      <xsl:with-param name="shiftS" select="0" as="xsd:integer"/>
      <xsl:with-param name="shiftP" select="0" as="xsd:integer"/>
    </xsl:call-template>

    <!-- Correspondances of the station -->
    <xsl:apply-templates select="correspondance">
      <xsl:with-param name="ppos"   select="position()" as="xsd:integer"/>
      <xsl:with-param name="shift"  select="0" as="xsd:integer"/>
    </xsl:apply-templates>
  </xsl:template>


  <!-- Group template -->
  <xsl:template match="group">
    <!-- Check which @style is this group -->
    <xsl:choose>
      <!-- Case split -->
      <xsl:when test="@style = 'split'">
        <xsl:apply-templates select="*">
          <xsl:with-param name="ppos"   select="position()" as="xsd:integer"/>
          <xsl:with-param name="shift"  select="0" as="xsd:integer"/>
        </xsl:apply-templates>
      </xsl:when>

      <!-- Case parallel -->
      <xsl:when test="@style = 'parallel'">
        <xsl:apply-templates select="*">
          <xsl:with-param name="ppos"   select="position()" as="xsd:integer"/>
          <xsl:with-param name="shift"  select="$gapY" as="xsd:integer"/>
        </xsl:apply-templates>
      </xsl:when>
    </xsl:choose>
  </xsl:template>


  <!-- Group's child : Station or None template -->
  <xsl:template match="group/*">
    <xsl:param name="ppos"/>  <!-- Parent position() (in the line) -->
    <xsl:param name="shift"/> <!-- Split := 0, Parallel := $gapY -->

    <xsl:choose>
      <!-- Top side of the group -->
      <xsl:when test="position() = 1">
        <xsl:call-template name="drawStation">
          <xsl:with-param name="pos"    select="$ppos" as="xsd:integer"/>
          <xsl:with-param name="shiftS" select="-$shift" as="xsd:integer"/>
          <xsl:with-param name="shiftP" select="-$gapY" as="xsd:integer"/>
        </xsl:call-template>

        <!-- Correspondances of the station -->
        <xsl:apply-templates select="correspondance">
          <xsl:with-param name="ppos"   select="$ppos" as="xsd:integer"/>
          <xsl:with-param name="shift"  select="-$gapY" as="xsd:integer"/>
        </xsl:apply-templates>
      </xsl:when>

      <!-- Bottom side of the group -->
      <xsl:when test="position() = 2">
        <xsl:call-template name="drawStation">
          <xsl:with-param name="pos"    select="$ppos" as="xsd:integer"/>
          <xsl:with-param name="shiftS" select="$shift" as="xsd:integer"/>
          <xsl:with-param name="shiftP" select="$gapY" as="xsd:integer"/>
        </xsl:call-template>

        <!-- Correspondances of the station -->
        <xsl:apply-templates select="correspondance">
          <xsl:with-param name="ppos"   select="$ppos" as="xsd:integer"/>
          <xsl:with-param name="shift"  select="$gapY" as="xsd:integer"/>
        </xsl:apply-templates>
      </xsl:when>
    </xsl:choose>
  </xsl:template>


  <!-- * * * * * * DRAW TEMPLATES * * * * * * -->


  <!-- Draw Station template -->
  <xsl:template name="drawStation">
    <xsl:param name="pos"/>    <!-- Current position() in the line -->
    <xsl:param name="shiftS"/> <!-- Shift for split -->
    <xsl:param name="shiftP"/> <!-- Shift for parallel -->

    <!-- Draw line to the back if is not the first station -->
    <xsl:choose>
      <!-- Preceding was a group but not current -->
      <xsl:when test="$pos != number(1) and
                      local-name(preceding-sibling::*[1]) = 'group'
                      and local-name() != 'group'">

        <!-- Join the lines -->
        <line x1="{($pos - 1) * $gapX + $x * 0.75}" y1="{$midY + $shiftS - $gapY + $x * 0.55}"
              x2="{$pos * $gapX - $x}"       y2="{$midY + $shiftP}"
              stroke="{$c}" stroke-width="7" stroke-linecap="round"/>

        <line x1="{($pos - 1) * $gapX + $x * 0.75}" y1="{$midY + $shiftS + $gapY - $x * 0.55}"
              x2="{$pos * $gapX - $x}"       y2="{$midY + $shiftP}"
              stroke="{$c}" stroke-width="7" stroke-linecap="round"/>
      </xsl:when>

      <!-- Simple straight line (Case : None as well) -->
      <xsl:when test="$pos != number(1)">

        <line x1="{($pos - 1) * $gapX + $x}" y1="{$midY + $shiftS}"
              x2="{$pos * $gapX}"       y2="{$midY + $shiftP}"
              stroke="{$c}" stroke-width="7" stroke-linecap="round"/>
      </xsl:when>
    </xsl:choose>

    <!-- Draw a dot for the station (not for None) -->
    <xsl:choose>
      <!-- Draw a white dot (with correspondance) -->
      <xsl:when test=".//local-name() = 'correspondance'">
        <circle cx="{$pos * $gapX}" cy="{$midY + $shiftP}" r="{$r}"
                fill="white" stroke="black" stroke-width="3"/>

        <!-- Draw correspondance downwards line -->
        <line x1="{$pos * $gapX}" y1="{$midY + $shiftP + $x - 5}"
              x2="{$pos * $gapX}" y2="{$midY + $shiftP + $x + 10}"
              stroke="black" stroke-width="1"/>
      </xsl:when>

      <!-- Draw a normal dot (no correspondances) -->
      <xsl:when test="local-name() != 'none'">
        <circle cx="{$pos * $gapX}" cy="{$midY + $shiftP}" r="{$r}"
                fill="{$c}" stroke="{$c}" stroke-width="3"/>
      </xsl:when>
    </xsl:choose>

    <!-- Draw station name -->
    <text transform="translate ({$pos * $gapX}, {$midY + $shiftP - $x}) rotate(-60)"
          font-family="Verdana" font-weight="bold" font-size="12" fill="black">
      <xsl:value-of select=".//nom"/>
    </text>

  </xsl:template>


  <!-- Draw Correspondances attached to the station -->
  <xsl:template match="correspondance">
    <xsl:param name="ppos"/>  <!-- Parent position() -->
    <xsl:param name="shift"/>

    <xsl:variable name="cc" select="fun:getColor(@ligne)"/>

    <circle cx="{$ppos * $gapX}"
            cy="{$midY + $shift + $x + 21 * position()}" r="{$r}"
            fill="{$cc}" stroke="{$cc}" stroke-width="3"/>

    <text x="{$ppos * $gapX - 4 * (string-length(.//@ligne))}"
          y="{$midY + $shift + 1.3*$x + 21 * position()}"
          font-family="serif" font-weight="bold" font-size="12" fill="black">
      <xsl:value-of select="@ligne"/>
    </text>

  </xsl:template>


  <!-- Function to get color -->
  <xsl:function name="fun:getColor" as="xsd:string">
    <xsl:param name="metroLine" as="xsd:string"/>

    <xsl:choose>
      <!-- Couleur Bouton d'Or -->
      <xsl:when test="$metroLine = '1'">
        <xsl:sequence select="'rgb(242, 201, 49)'"/>
      </xsl:when>

      <!-- Couleur Azur -->
      <xsl:when test="$metroLine = '2'">
        <xsl:sequence select="'rgb(33, 110, 180)'"/>
      </xsl:when>

      <!-- Couleur Olive -->
      <xsl:when test="$metroLine = '3'">
        <xsl:sequence select="'rgb(154, 153, 64)'"/>
      </xsl:when>

      <!-- Couleur Parme -->
      <xsl:when test="$metroLine = '4'">
        <xsl:sequence select="'rgb(188, 77, 152)'"/>
      </xsl:when>

      <!-- Couleur Orange -->
      <xsl:when test="$metroLine = '5'">
        <xsl:sequence select="'rgb(222, 139, 83)'"/>
      </xsl:when>

      <!-- Couleur Menthe -->
      <xsl:when test="$metroLine = '6'">
        <xsl:sequence select="'rgb(121, 187, 164)'"/>
      </xsl:when>

      <!-- Couleur Rose -->
      <xsl:when test="$metroLine = '7'">
        <xsl:sequence select="'rgb(223, 154, 177)'"/>
      </xsl:when>

      <!-- Couleur Lilas -->
      <xsl:when test="$metroLine = '8'">
        <xsl:sequence select="'rgb(197, 163, 202)'"/>
      </xsl:when>

      <!-- Couleur Acacia -->
      <xsl:when test="$metroLine = '9'">
        <xsl:sequence select="'rgb(205, 200, 63)'"/>
      </xsl:when>

      <!-- Couleur Ocre -->
      <xsl:when test="$metroLine = '10'">
        <xsl:sequence select="'rgb(223, 176, 57)'"/>
      </xsl:when>

      <!-- Couleur Marron -->
      <xsl:when test="$metroLine = '11'">
        <xsl:sequence select="'rgb(142, 101, 56)'"/>
      </xsl:when>

      <!-- Couleur Sapin -->
      <xsl:when test="$metroLine = '12'">
        <xsl:sequence select="'rgb(50, 142, 91)'"/>
      </xsl:when>

      <!-- Couleur Pervenche -->
      <xsl:when test="$metroLine = '13'">
        <xsl:sequence select="'rgb(137, 199, 214)'"/>
      </xsl:when>

      <!-- Couleur Iris -->
      <xsl:when test="$metroLine = '14'">
        <xsl:sequence select="'rgb(103, 50, 142)'"/>
      </xsl:when>

      <!-- Couleur Pervenche -->
      <xsl:when test="$metroLine = '3B'">
        <xsl:sequence select="'rgb(137, 199, 214)'"/>
      </xsl:when>

      <!-- Couleur Menthe -->
      <xsl:when test="$metroLine = '7B'">
        <xsl:sequence select="'rgb(121, 187, 164)'"/>
      </xsl:when>

      <!-- Defauilt color is green -->
      <xsl:otherwise>
        <xsl:sequence select="'green'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

</xsl:stylesheet>